from flask import Flask
from flask import render_template
app = Flask(__name__)

@app.route('/')
def index():
	return '<h1>This is index</h1>'

#只接收int类型
@app.route('/int/<int:integer>')
def inttype(integer):
	return '<h1> id is %d </h1>' % integer

#只接收float类型
@app.route('/float/<float:decimal>')
def floattype(decimal):
	return '<h1> id is %f </h1>' % decimal

#接受任何字符串，包括其中包含/的
@app.route('/path/<path:anything>')
def pathtype(anything):
	return '<h1> anything is %s </h1>' % anything

#可以使用尖括号使用多个路径参数
@app.route('/combine/<firstname>/<secondname>/')
def combine(firstname,secondname):
	return '<h1>' + firstname + ' ' + secondname + '</h1>'

#使用修饰器定义错误页面
@app.errorhandler(404)
def page_not_found(e):
	return render_template('404.html',404)

@app.errorhandler(500)
def internal_server_error(e):
	return render_template('500.html',500)

if __name__ == '__main__':
	app.run()

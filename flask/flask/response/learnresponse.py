from flask import Flask
from flask import abort, redirect, make_response
app = Flask(__name__)

@app.route('/header')
def header():
    return ('<h1>Header</h1>',200, {'User-Agent':'Wangjin'})

@app.route('/response')
def resp():
	#make_response同上也可接收三个参数
	response = make_response('<h1>RESPONSE</h1>')
	response.set_cookie('user','wangjin')
	return response

@app.route('/error')
def error():
	abort(404)

@app.route('/redirect')
def redir():
	return redirect('http://www.baidu.com')


if __name__ == '__main__':
    app.run(debug=True)

from flask import Flask, request, redirect, flash, url_for, render_template
app = Flask(__name__)
app.config['SECRET_KEY'] = 'hard to guess some str'

@app.route('/', methods=['GET', 'POST'])
def index():
    name = request.form.get('name')
    if name:
        flash('Hello ' + name)
    else:
        return render_template('index.html')
    return redirect(url_for('index'))


if __name__ == '__main__':
    app.run(debug=True)

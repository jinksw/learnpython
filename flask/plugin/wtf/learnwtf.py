#pip install flask-wtf
from flask import Flask, render_template

app = Flask(__name__)
#需要设置密钥来防止跨站请求伪造
app.config['SECRET_KEY']='hard to guess string'

#首先需要定义表单类
from flask.ext.wtf import Form
from wtforms import StringField, SubmitField
from wtforms.validators import Required

class NameForm(Form):
    name = StringField('What is your name?', validators=[Required()])
    submit = SubmitField('Submit')

#然后定义表单html 如table.html


@app.route('/', methods=['GET', 'POST'])
def index():
    name = None
    form = NameForm()
    if form.validate_on_submit():
        name = form.name.data
        form.name.data = ''
    return render_template('table.html', form=form, name=name)


if __name__ == '__main__':
    app.run(debug=True)
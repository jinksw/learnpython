#安装
#pip install flask-script
from flask import Flask
from flask.ext.script import Manager, Shell
app = Flask(__name__)
manager = Manager(app)

#manager可以包装shell使shell命令自动导入一些变量
test = {'a':1}
def make_shell_context():
	return dict(app=app, test=test)
manager.add_command('shell', Shell(make_context=make_shell_context))
#可以通过运行python learnscript.py shell 然后键入test回车测试是否完成自动导入变量

#还可以通过@manager.command修饰器来增加命令
@manager.command
def test():
	print('run test')
#可以通过python learnscript.py test来验证

if __name__ == '__main__':
	manager.run()

#python learnscript.py runserver 将以调试模式启动Web服务器
#python learnscript.py shell 可以启动Python shell回话,进行测试或调试

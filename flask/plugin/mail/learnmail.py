#pip3 install flask-mail
from flask import Flask
from flask.ext.script import Manager
from flask.ext.mail import Mail
from flask.ext.script import Shell
from flask.ext.mail import Message
import os

app = Flask(__name__)
#note that the config must be configured before initial mail instance
#默认配置           默认值
#MAIL_SERVER    localhost
#MAIL_PORT      25
#MAIL_USE_TLS   False
#MAIL_USE_SSL   False
#MAIL_USERNAME  None
#MAIL_PASSWORD  None
app.config['MAIL_SERVER'] = 'smtp.qq.com'
app.config['MAIL_PORT'] = 25
app.config['MAIL_USE_SSL'] = False
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USERNAME'] = os.environ.get('MAIL_USERNAME')
app.config['MAIL_PASSWORD'] = os.environ.get('MAIL_PASSWORD')

manager = Manager(app)
mail = Mail(app)

def make_shell_context():
    return dict(run=runFromShell, send_email=send_email)

manager.add_command('shell', Shell(make_context=make_shell_context))


#python learnmail.py shell
#from flask.ext.mail import Message
#from learnmail import mail
#msg = Message('test subject', sender='jinksw@vip.qq.com', recipients=['wj358273941@163.com'])
#msg.body='text body'
#msg.html='<b>HTML</b> body'
#with app.app_context():
#	mail.send(msg)

def runFromShell():
    msg = Message('test subject', sender='jinksw@vip.qq.com', recipients=['wj358273941@163.com'])
    msg.body='text body'
    msg.html='<b>HTML</b> body'
    with app.app_context():
       mail.send(msg)

#import jinja
from flask import render_template
app.config['FLASKY_MAIL_SUBJECT_PREFIX'] = '[FLASKY]'
app.config['FLASKY_MAIL_SENDER'] = 'Flasky Admin <jinksw@vip.qq.com>'

def send_email(to, subject, template, **kwargs):
    msg = Message(app.config['FLASKY_MAIL_SUBJECT_PREFIX'] + subject,
    sender = app.config['FLASKY_MAIL_SENDER'],
    recipients = [to])
    msg.body = render_template(template + '.txt', **kwargs)
    msg.html = render_template(template + '.html', **kwargs)
    mail.send(msg)


if __name__ == '__main__':
	manager.run()
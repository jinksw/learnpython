# -*- coding: utf-8 -*-
#pip install flask-script
#pip install flask-sqlalchemy
#flask-migrate用于处理数据库迁移
#pip install flask-migrate
from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.script import Manager
from flask.ext.script import Shell
from flask.ext.migrate import Migrate, MigrateCommand
import os

basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)
manager = Manager(app)


#为shell添加上下文
def make_shell_context():
	return dict(app=app, db=db, User=User, Role=Role)

manager.add_command("shell", Shell(make_context=make_shell_context))


#配置数据库URI
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'data.sqlite')
app.config['SQLALCHEMY_COMMIT_ON_TEARDOWN'] = True

db = SQLAlchemy(app)
migrate = Migrate(app, db)
#将迁移类绑定到db命令上
manager.add_command('db', MigrateCommand)
#python learnsqlalchemy.py db init 创建迁移仓库
#python learnsqlalchemy.py db migrate -m 'initial migration' 自动创建迁移脚本
#python learnsqlalchemy.py db upgrade 把迁移应用到数据库中



#继承db.Model类
class Role(db.Model):
	#指定表名
	__tablename__ = 'roles'
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(64), unique=True)
	#一个角色对应多个用户 
	#第一个参数代表这个关系的另一端是哪个模型
	#backref代表需要在User模型中添加一个指向role的属性
	users = db.relationship('User', backref='role', lazy='dynamic')

	def __repr__(self):
		return '<Role %r>' % self.name

class User(db.Model):
	__tablename__ = 'users'
	id = db.Column(db.Integer, primary_key=True)
	username = db.Column(db.String(64), unique=True, index=True)
	#定义外键
	role_id = db.Column(db.Integer, db.ForeignKey('roles.id'))

	def __repr__(self):
		return '<User %r>' % self.username


#python learnsqlalchemy.py shell
#from learnsqlalchemy import db, Role, User
def runFromTheShell():
	db.drop_all()
	db.create_all()

	admin_role = Role(name='Admin')
	mod_role = Role(name='Moderator')
	user_role = Role(name='User')
	user_john = User(username='john',role=admin_role)
	user_susan = User(username='susan',role=user_role)
	user_david = User(username='david',role=user_role)

	#添加进事务
	db.session.add(mod_role)
	db.session.add(user_role)
	db.session.add(user_john)
	db.session.add(user_susan)
	db.session.add(user_david)
	#提交事务
	db.session.commit()

	#更新模型
	admin_role.name = 'Administrator'
	db.session.add(admin_role)
	db.session.commit()

	#删除
	db.session.delete(mod_role)
	db.session.commit()

	#查询
	Role.query.all()
	User.query.all()

	#找到所有role为User的用户
	User.query.filter_by(role=user_role).all()

	#当Role的users属性被定义成lazy为dynamic时,调用users会返回一个query对象,而不是User list.
	#按顺序返回role为User的用户
	user_role.users.order_by(User.username).all()



if __name__ == '__main__':
	manager.run()
from flask import Flask
from flask.ext.script import Manager
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.script import Shell
from flask.ext.migrate import Migrate, MigrateCommand
import os



app = Flask(__name__)
manager = Manager(app)
basedir = os.path.abspath(os.path.dirname(__file__))
#配置数据库URI
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'data.sqlite')
app.config['SQLALCHEMY_COMMIT_ON_TEARDOWN'] = True
db = SQLAlchemy(app)
migrate = Migrate(app, db)

def make_shell_context():
	return dict(app=app, db=db, User=User, Role=Role)
manager.add_command("shell", Shell(make_context=make_shell_context))
manager.add_command("db", MigrateCommand)

#继承db.Model类
class Role(db.Model):
	#指定表名
	__tablename__ = 'roles'
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(64), unique=True)
	haha = db.Column(db.String(64))
	#一个角色对应多个用户 
	#第一个参数代表这个关系的另一端是哪个模型
	#backref代表需要在User模型中添加一个指向role的属性
	users = db.relationship('User', backref='role', lazy='dynamic')

	def __repr__(self):
		return '<Role %r>' % self.name

class User(db.Model):
	__tablename__ = 'users'
	id = db.Column(db.Integer, primary_key=True)
	username = db.Column(db.String(64), unique=True, index=True)
	#定义外键
	role_id = db.Column(db.Integer, db.ForeignKey('roles.id'))

	def __repr__(self):
		return '<User %r>' % self.username


if __name__ == '__main__':
	manager.run()


#python learnsqlalchemy.py db init 创建迁移仓库
#python learnsqlalchemy.py db migrate -m 'initial migration' 自动创建迁移脚本
#python learnsqlalchemy.py db upgrade 把迁移应用到数据库中
#遇到数据库改动时,先修改模型,再使用第二条提交改动,最后使用第三条应用到数据库中
